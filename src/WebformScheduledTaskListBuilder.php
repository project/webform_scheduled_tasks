<?php

namespace Drupal\webform_scheduled_tasks;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A scheduled task list builder.
 */
final class WebformScheduledTaskListBuilder extends ConfigEntityListBuilder {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new FilterFormatListBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, ConfigFactoryInterface $config_factory, MessengerInterface $messenger, RouteMatchInterface $routeMatch, DateFormatterInterface $dateFormatter) {
    parent::__construct($entity_type, $storage);

    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->routeMatch = $routeMatch;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('current_route_match'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $associated_webform = $this->routeMatch->getParameter('webform');
    $query = $this->getStorage()->getQuery()
      ->condition('webform', $associated_webform, '=')
      ->sort($this->entityType->getKey('id'))
      ->accessCheck();
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'label' => $this->t('Task name'),
      'type' => $this->t('Task type'),
      'result_set' => $this->t('Result set'),
      'status' => $this->t('Status'),
      'next_scheduled_run' => $this->t('Next scheduled run'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\webform_scheduled_tasks\Entity\WebformScheduledTaskInterface $entity */
    return [
      'label' => $entity->label(),
      'type' => $entity->getTaskPlugin()->getPluginDefinition()['label'],
      'result_set' => $entity->getResultSetPlugin()->getPluginDefinition()['label'],
      'status' => $entity->isHalted() ? $this->t('Halted') : $this->t('Active'),
      'next_scheduled_run' => $entity->getNextTaskRunDate() ? $this->dateFormatter->format($entity->getNextTaskRunDate()) : $this->t('Unscheduled'),
    ] + parent::buildRow($entity);
  }

}
